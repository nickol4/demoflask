FROM python:latest

RUN pip install flask
RUN pip install Flask-restfull

WORKDIR /app

COPY main.py ./

RUN main.py
